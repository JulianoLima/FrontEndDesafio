import React, {Component} from 'react';
import Table from 'rc-table';
import 'rc-table/assets/index.css';

const columns = [
    {title: 'codigo', dataIndex: 'codigo'},
    {title: 'Data Cadastro', dataIndex: 'data_cadastro'},
    {title: 'Titulo', dataIndex: 'titulo'},
    {title: 'Departamento', dataIndex: 'departamento'},
    {title: 'Categoria', dataIndex: 'categoria'},
];


class TableFiles extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
        }
    }

    componentWillReceiveProps() {
        this.setState({data: this.props.data})
    }

    render() {

        return (
            <Table columns={columns}
                   data={this.props.data}
            >
            </Table>
        );
    }
}

export default TableFiles;