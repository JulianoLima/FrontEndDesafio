import React, {Component} from 'react';
import {Container} from 'bloomer';
import Header from './Header';


class Layout extends Component {
    render() {
        return (
            <Container isFluid style={{width: '100%'}}>
                <Header/>
                {this.props.children}
            </Container>
        );
    }
}

export default Layout;