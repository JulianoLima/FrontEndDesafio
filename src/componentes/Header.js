import React, {Component} from 'react';
import {Navbar, NavbarGroup, NavbarHeading, NavbarDivider, Alignment} from '@blueprintjs/core';
import {Link} from 'react-router-dom';


class Header extends Component {

    render() {

        return (
            <Navbar>
                <NavbarGroup align={Alignment.LEFT}>
                    <NavbarHeading>Gerenciador Documentos</NavbarHeading>
                    <NavbarDivider/>
                    <Link to={'/cadastro'}> Cadastro de Documentos
                    </Link>
                    <NavbarDivider/>
                    <Link to={'/'}> Lista Documentos
                    </Link>
                </NavbarGroup>
                {this.props.children}
            </Navbar>

        );
    }
}

export default Header;