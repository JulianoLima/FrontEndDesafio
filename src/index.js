import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Layout from './componentes/Layout';
import RegisterFiles from './paginas/RegisterDocuments';
import Home from './paginas/Home';
import '../node_modules/toastr/build/toastr.min.css';
import '../node_modules/@blueprintjs/core/lib/css/blueprint.css';


ReactDOM.render(<Router>
        <Layout>
            <Switch>
                <Route exact path="/" component={Home}/>
                <Route path="/cadastro" component={RegisterFiles}/>
            </Switch>
        </Layout>

    </Router>,
    document.getElementById('root'));
registerServiceWorker();
