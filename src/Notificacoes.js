import Toastr from 'toastr';

/**
 * Definições do Componente notificações
 * @type {string}
 */
Toastr.showDuration='100';
Toastr.options.timeOut='1000';
Toastr.options.preventDuplicates = true;
Toastr.options.closeButton= 'true';

export default Toastr;