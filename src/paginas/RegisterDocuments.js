import React, {Component} from 'react';
import {FormGroup, InputGroup, Button, Alignment} from '@blueprintjs/core';
import {Container} from 'bloomer';
import API from '../API';
import Notificacoes from '../Notificacoes';

class RegisterFiles extends Component {

    constructor(props) {
        super(props);
        this.state = {
            codigo: '',
            categoria: '',
            departamento: '',
            titulo: ''
        };

        this.handleChangeCodigo = this.handleChangeCodigo.bind(this);
        this.handleChangeCategoria = this.handleChangeCategoria.bind(this);
        this.handleChangeDepartamento = this.handleChangeDepartamento.bind(this);
        this.handleChangeTitulo = this.handleChangeTitulo.bind(this);
        this.cleanValues = this.cleanValues.bind(this);

    }

    handleChangeCodigo(e) {
        this.setState({codigo: e.target.value});
    }

    handleChangeCategoria(e) {
        this.setState({categoria: e.target.value});
    }

    handleChangeDepartamento(e) {
        this.setState({departamento: e.target.value})
    }

    handleChangeTitulo(e) {
        this.setState({titulo: e.target.value});
    }

    cleanValues() {
        this.setState({codigo: '', categoria: '', departamento: '', titulo: ''});
    }


    onSubmit(e) {
        e.preventDefault();
        API.post('/documento/salva', {
            data: {
                'codigo': this.state.codigo,
                'titulo': this.state.titulo,
                'departamento': this.state.departamento,
                'categoria': this.state.categoria
            }
        }).then(() => {
            Notificacoes.success("Sucesso no cadastro")
            this.cleanValues();

        }).catch((erro) => {
            Notificacoes.error('Mensagem de erro', erro);
        });
    }


    render() {
        return (
            <Container>
                <FormGroup>
                    <FormGroup
                        label="Código"
                        labelFor="text-input"
                        requiredLabel={false}
                    >
                        <InputGroup id="codigo-input"
                                    placeholder="Digite o código do documento"
                                    ref={(Input) => this.codigo = Input}
                                    value={this.state.codigo}
                                    onChange={this.handleChangeCodigo}
                        />
                    </FormGroup>
                    <FormGroup
                        label="Título"
                        labelFor="text-input"
                        requiredLabel={false}
                    >
                        <InputGroup
                            id="titulo-input"
                            placeholder="Digite o título do documento"
                            fill={false}
                            onChange={this.handleChangeTitulo}
                        />
                    </FormGroup>
                    <FormGroup
                        label="Departamento"
                        labelFor="text-input"
                        requiredLabel={false}
                    >
                        <InputGroup id="departamento-input"
                                    placeholder="Digite o Departamento do documento"
                                    fill={false}
                                    onChange={this.handleChangeDepartamento}
                        />
                    </FormGroup>
                    <FormGroup
                        label="Categoria"
                        labelFor="text-input"
                        requiredLabel={false}
                    >
                        <InputGroup
                            id="categoria-input"
                            placeholder="Digite a categoria do documento"
                            fill={false}
                            onChange={this.handleChangeCategoria}
                        />
                    </FormGroup>
                    <Button
                        onClick={this.onSubmit.bind(this)}
                        className={"pt-intent-success"}
                        type={"submit"}
                        alignText={Alignment.RIGHT}>
                        Salvar
                    </Button>
                </FormGroup>
            </Container>
        );
    }
}

export default RegisterFiles;