import React, {Component} from 'react';
import TableFiles from '../componentes/TableDocuments';
import API from '../API';
import Notificacoes from '../Notificacoes';


class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataTable: [{}]
        }
    };

    componentWillMount() {
        API.get('/documento/lista').then((res) => {
            this.setState({dataTable: res.data});
        }).catch((err) => {
            Notificacoes.error('Erro de conexão à base de dados', err).options={
                "closeButton": true,
                "hideDuration": "4000",
            };
            console.log('retorno do erro', err);
        })
    }

    render() {
        return (
            <div style={{padding: '2px'}}>
                <TableFiles data={this.state.dataTable}/>
            </div>
        );
    }
}

export default Home;